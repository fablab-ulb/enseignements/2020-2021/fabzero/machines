# Guide Epilog Fusion Pro 32

## Spécifications

* Surface de découpe : 81 x 50 cm
* Hauteur maximum : 31 cm
* Puissance du LASER : 60 W
* Type de LASER : Tube CO2 (infrarouge)

## Envoi d'un travail à la machine

### Ouvrir le fichier dans Inkscape

Si le format de fichier n'est pas SVG, importez-le dans Inkscape (Fichier -> Importer...).

Pour avoir la découpe la plus fine possible, réduisez l'épaisseur des traits à 0,01 mm.

### Imprimer

La découpeuse LASER doit être allumée.

![](/img/print.png)

![](/img/printer.png)

Cliquez sur le bouton **Imprimer** et attendez l'ouverture du Dashboard Epilog.

Si rien ne se passe, démarrez le Dashboard Epilog manuellement via l'icône **Epilog** sur le bureau.

### Paramétrage

Commencez par donner un nom à votre travail :

![](/img/name.png)

Pour la découpe (ou la gavure vectorielle), sélectionnez **Vector** dans la liste **Process Type**.

N'utilisez l'option **Engrave** que si vous voulez graver une image matricielle.

![](/img/vector.png)

Réglez la vitesse, la puissance et la fréquence.

Pour que la machine rêgle automatiquement le focus sur la surface du matériau, sélectionnez **Plunger** dans la liste **Auto-focus**.

Attention, cette option fonctionne mal avec des matériaux fragiles (papier, textiles, ...).

![](/img/focus.png)

Avant d'envoyer le travail à la machine, positionnez le dessin à l'endroit voulu.

Attention, tout ce qui dépasse les pointillés roses ne sera pas découpé.

Il est possible d'élargir la zone de travail en déplaçant les lignes roses.

Si tout est prêt, appuyez sur le bouton **Print** :

![](/img/send.png)

### Démarrage

Le travail apparaît alors en haut de la liste sur l'écran de la machine.

Pour démarrer la découpe, sélectionnez-le puis appuyez sur "Play" (bouton noir et blanc).

![](/img/button.jpg)

## Réglages

Vous pouvez trouver les réglages de vitesse et puissance dans ce [tableau des réglages en fonction des matériaux](https://www.epiloglaser.com/assets/downloads/fusion-material-settings.pdf), dans la colonne **60 watt**.

Pour la découpe (vector), le réglage de la fréquence est aussi indiqué.

Pour la gravure (engrave), c'est la résolution en DPI qui est indiquée.
